\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{VUbeamer}[2015/09/18 for beamer 3.01]

\LoadClass[xcolor=dvipsnames]{beamer}

\usecolortheme[named=Blue]{structure}
\usetheme{Antibes}
\setbeamertemplate{items}[ball]
\setbeamertemplate{blocks}[rounded][shadow=true]

\RequirePackage{graphics}
\RequirePackage{subfigure}
\RequirePackage{wrapfig}
\RequirePackage{lmodern}
\RequirePackage{kvoptions}
\RequirePackage{media9}
\RequirePackage{epstopdf}


\DeclareStringOption[]{conf}[Conference]
\DeclareStringOption[ ]{group}[Group]
\DeclareStringOption[]{dept}[Department]
\ProcessKeyvalOptions*

\def\insertconference{\VUbeamer@conf}
\institute[Villanova]{{\VUbeamer@group \\ \VUbeamer@dept} \\
                 {Villanova University} \\ \vspace*{0.1in}
                 \includegraphics[width=0.15\textwidth]{eng_logo_edited.pdf}}
\logo{\includegraphics[width=1.0cm]{vuseal.pdf}} %little logo in corner of slides

\RequirePackage{beamerouterthemevuheadline}
\RequirePackage{beamerouterthemevufootline}



%Make the table of contents show progress whenever the section/subsection changes
\AtBeginSection[]
{
   \begin{frame}
       \frametitle{Outline}
       \tableofcontents[
        currentsection,
        hideothersubsections
       ]
   \end{frame}
}

%Environment 'halfpage' calls a minipage at 45% linewidth, so a slide can quickly and easily be split into two regions
\newenvironment{halfpage}
  {\begin{minipage}{0.45\linewidth}}
  {\end{minipage}}

\newcommand{\embedvideo}[1]{
  \begin{center}
    \includemedia[width=0.9\linewidth, height=0.5\linewidth, activate=pageopen, addresource=#1, flashvars={source=#1}]{}{VPlayer.swf}
  \end{center}
}