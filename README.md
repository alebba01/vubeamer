# README #

This package is for setting up a LaTeX class for beamer presentations with a Villanova University College of Engineering theme.

*The installation instructions assume that the user has already installed and built a presentation using the beamer class.*

### What is this repository for? ###

* Creating LaTeX presentations
* Minimizing in document formatting code
* Globalizing class setup so the file does not need to be copied to the local directory of each presentation

### Added Functionality ###

* documentclass option 'conf' sets the conference the presentation is for, blank if left omitted
* documentclass option 'group' sets the lab group the presenter represents, blank if left omitted
* documentclass option 'dept' sets the department the presenter represents, blank if left omitted
* environment 'halfpage' creates a minipage environment with 45% linewidth, to quickly divide the slide into two equal size regions (useful for including figures)
* command 'embedvideo' centers and embeds a video occupying most of the slide using the media9 package - ex call: \embedvideo{video.mp4}


### How do I get set up? ###

* First, ensure your system has the beamer class for LaTeX (usually a standard package, if not it can be found [here](https://bitbucket.org/rivanvx/beamer/wiki/Home))
* Locate the directory where the beamer class is installed 
	* On windows 8 / 10 with MiKTeX, it is generally C:\Program Files (x86)\MiKTeX 2.9\tex\latex\beamer
	* On older windows distros it has also been found in %HOME%\AppData\Roaming\MiKTeX\tex\latex
* The repository will need to go into this directory, but it will likely have permissions
    * Clone this repository into a folder temporarily
    * Cut and past the repository one level above the level that contains the beamer.cls file. (for the same setup as described above, the beamer.cls file is within beamer\base, so clone the VUbeamer repo into beamer\)
* Next, the file name database for your LaTeX compilation needs to be refreshed. 
    * On windows 8 / 10 with MiKTeX, this can be done by opening 'MiKTeX settings (Admin)' and selecting 'Refresh FNDB'. 
    * On windows 7 with MiKTeX, open your start menu > programs > MiKTeX 2.9 > Maintenance (Admin) > Settings (Admin), and select 'Refresh FNDB'.
* Your setup should be complete! There is a template file in the repository that can be used to outline the basic calls for the class. Copy and paste that into any other directory and check to make sure it builds successfully.

### Contribution guidelines ###

* Please let me know if there are any issues, or functionality that could be helpful. This is specifically targeted for a VU college of Engineering userbase, so I do not have any intention of making it modular for other schools - feel free to fork and modify if that is your goal.

### Who do I talk to? ###

* Anderson Lebbad - alebba01@villanova.edu